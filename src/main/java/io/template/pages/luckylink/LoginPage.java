package io.template.pages.luckylink;

import io.template.lib.Init;
import io.template.lib.Props;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Login")
public class LoginPage extends AnyPage {

    @FindBy(css = "input[type='email']")
    @ElementTitle("login")
    public WebElement inputLogin;

    @FindBy(css = "input[type='password']")
    @ElementTitle("password")
    public WebElement inputPassword;

    @FindBy(css = "button[type='submit']")
    @ElementTitle("button login")
    public WebElement buttonLogin;

    public LoginPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.elementToBeClickable(inputLogin));
    }

    public void login_by_agent() throws Exception {
        fill_field(inputLogin, Props.get("agent.login"));
        fill_field(inputPassword, Props.get("agent.password"));
        press_button_by_js(buttonLogin);
    }

}
