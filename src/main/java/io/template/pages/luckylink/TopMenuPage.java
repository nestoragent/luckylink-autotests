package io.template.pages.luckylink;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
public abstract class TopMenuPage extends AnyPage{

    @FindBy(css = "ul.navbar-nav li.active")
    @ElementTitle("Active menu item")
    public WebElement liActiveMenuItem;


}
