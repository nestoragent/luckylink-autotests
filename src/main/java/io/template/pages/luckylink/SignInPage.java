package io.template.pages.luckylink;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Sign In")
public class SignInPage extends AnyPage {

    @FindBy(css = "a.btn-social.btn-default")
    @ElementTitle("Sign in with Email")
    public WebElement buttonSignInByEmail;

    public SignInPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.elementToBeClickable(buttonSignInByEmail));
    }
}
