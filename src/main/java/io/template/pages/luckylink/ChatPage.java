package io.template.pages.luckylink;

import io.template.lib.Init;
import io.template.lib.pageFactory.ElementTitle;
import io.template.lib.pageFactory.PageEntry;
import io.template.pages.AnyPage;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Created by Nestor on 22.03.2016.
 */
@PageEntry(title = "Chat")
public class ChatPage extends TopMenuPage {

    @FindBy(css = "div.messenger-container")
    @ElementTitle("init")
    public WebElement initElem;

    public ChatPage() {
        Init.getDriverExtensions().waitForPageToLoad();
        PageFactory.initElements(Init.getDriver(), this);
        new WebDriverWait(Init.getDriver(), Init.getTimeOutInSeconds()).
                until(ExpectedConditions.elementToBeClickable(initElem));
        System.out.println("href: " + liActiveMenuItem.findElement(By.xpath("./a")).getAttribute("href"));
        Assert.assertTrue(liActiveMenuItem.findElement(By.xpath("./a")).getAttribute("href").contains("/messenger/agent"));
    }
}
